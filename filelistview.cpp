#include <QtGui>
#include "filelistview.h"

#define is ==
#define isnt !=





void File_list_view::set_text_font(QPainter& painter)
{
	#ifdef Q_OS_MAC
	painter.setFont(QFont("Optima", 18));
	#else
	#ifdef Q_OS_WIN32
	painter.setFont(QFont("Segoe UI", 10));
	#else
	painter.setFont(QFont("Arial", 10));
	#endif
	#endif
}





File_list_view::File_list_view(QWidget *parent) :
	QWidget(parent),
	m_padding(4, 4),
	m_spacing(18, 4),

	m_items_source(),

	m_items(),
	m_columns(),
	m_view_size_used_for_last_recalc(0, 0),

	m_current_path(),
	m_left_column(0),
	m_focused_item_index(0)

{
}





QFileInfoList get_sorted_file_entries(QString path)
{
	QDir dir(path);
	QFileInfoList entries = dir.entryInfoList(QDir::NoFilter, QDir::DirsFirst | QDir::Name | QDir::IgnoreCase);
	bool is_root = dir.isRoot();
	for(int i = 0; i < entries.length();) {
		QString file_name(entries[i].fileName());
		if(file_name is ".") {
			entries.removeAt(i);
		}
		else if(file_name is "..") {
			if(is_root) {
				entries.removeAt(i);
			}
			else {
				entries.move(i, 0);
				i++;
			}
		}
		else {
			i++;
		}
	}
	return entries;
}





int find_parent_entry_index(QString child_path, QString parent_entries_path, QFileInfoList& parent_entries)
{
	child_path = QDir::cleanPath(child_path);
	parent_entries_path = QDir::cleanPath(parent_entries_path);
	if(not child_path.startsWith(parent_entries_path, Qt::CaseInsensitive)) {
		return -1;
	}
	for(int i = 0; i < parent_entries.length(); i++) {
		QFileInfo& parent_entry = parent_entries[i];
		if(parent_entry.isDir()) {
			QString parent_path = parent_entries_path + "/" + parent_entry.fileName();
			if(child_path.startsWith(parent_path)) {
				return i;
			}
		}
	}
	return -1;
}





void File_list_view::set_current_path(QString path)
{
	QString new_path = QDir::cleanPath(path);
	if(new_path is m_current_path) {
		return;
	}

	QString prev_path = m_current_path;

	m_items_source = get_sorted_file_entries(new_path);
	m_current_path = new_path;
	m_focused_item_index = find_parent_entry_index(prev_path, m_current_path, m_items_source);
	if(m_focused_item_index is -1) {
		m_focused_item_index = 0;
	}

	m_items.clear();
	m_columns.clear();
	m_left_column = 0;
	m_view_size_used_for_last_recalc = QSize(0, 0);
	update();
	set_filter("");

	emit current_path_changed(m_current_path);
	emit filter_changed("");
}





void File_list_view::recalc_items_and_columns_if_needed(QSize view_size, QPainter& painter)
{
	if(m_items.length() isnt m_items_source.length()) {
		recreate_items(painter);
	}

	if(view_size isnt m_view_size_used_for_last_recalc) {
		recalc_layout(view_size);
	}
}





void File_list_view::recalc_items_and_columns_if_needed()
{
	if(m_items.length() isnt m_items_source.length()) {
		QPainter painter(this);
		recreate_items(painter);
	}

	if(size() != m_view_size_used_for_last_recalc) {
		recalc_layout(size());
	}
}





void File_list_view::recreate_items(QPainter& painter)
{
	m_items.clear();
	set_text_font(painter);
	QFontMetrics fm(painter.font());

	for(int i = 0; i < m_items_source.length(); i++) {
		QString name = m_items_source[i].fileName();
		m_items.append(Item(name, fm.size(Qt::TextSingleLine, name)));
	}
}





void File_list_view::recalc_layout(QSize view_size)
{
	m_view_size_used_for_last_recalc = view_size;
	m_columns.clear();
	if(m_items_source.isEmpty()) {
		return;
	}
	Wrapper wrapper(view_size.height(), m_padding.width(),m_padding.height(), m_spacing.width(), m_spacing.height());
	for(int i = 0; i < m_items_source.length(); i++) {
		Item& item = m_items[i];
		int x, y;
		wrapper.next(item.rect.width(), item.rect.height(), x, y, m_columns);
		item.rect.setRect(x, y, item.rect.width(), item.rect.height());
		item.column = m_columns.length() - 1;
	}
}





QRect File_list_view::rect_to_view(QRect r)
{
	return r.translated(-(m_columns[m_left_column].pos - m_padding.width()), 0);
}





QRect File_list_view::item_view_rect(int item_index)
{
	return rect_to_view(m_items[item_index].rect);
}





QRect File_list_view::column_rect(int column_index)
{
	QRect r = rect();
	Wrapper::Strip& column = m_columns[column_index];
	r.setLeft(column.pos);
	r.setWidth(column.size);
	return r;
}





QRect File_list_view::column_view_rect(int column_index)
{
	return rect_to_view(column_rect(column_index));
}




bool File_list_view::is_filtered(int item_index) const
{
	return in_range(item_index) and m_items[item_index].text.contains(m_filter, Qt::CaseInsensitive);
}





bool File_list_view::in_range(int item_index) const
{
	return item_index >= 0 and item_index < m_items.length();
}





void File_list_view::paintEvent(QPaintEvent * evt)
{
	QPainter painter(this);
	recalc_items_and_columns_if_needed(size(), painter);
	QColor bg(palette().color(QPalette::Background));

	QFontMetrics fm(painter.font());
	for(int i = 0; i < m_items_source.length(); i++) {
		Item& item = m_items[i];

		QRect r = item_view_rect(i);
		if (!r.intersects(evt->rect())) {
			continue;
		}
		if(i is m_focused_item_index) {
			painter.setPen(Qt::red);
			painter.drawRect(r.adjusted(-2,-2,2,2));
		}

		QFileInfo& item_source = m_items_source[i];
		if(item_source.isDir()) {
			painter.setPen(Qt::white);
		}
		else {
			painter.setPen(Qt::gray);
		}
		set_text_font(painter);
		painter.drawText(r, Qt::AlignLeft, item.text);

		if(not m_filter.isEmpty()) {
			int filter_pos = item.text.indexOf(m_filter , 0, Qt::CaseInsensitive);
			if(filter_pos >= 0) {
				QString left_text = item.text.left(filter_pos);
				QString filtered_text = item.text.mid(filter_pos, m_filter.length());
				QRectF rf(r);
				QRectF left_rect = painter.boundingRect(rf, Qt::AlignLeft, left_text);
				QRectF filtered_rect = painter.boundingRect(rf.translated(left_rect.width(), 0), Qt::AlignLeft, filtered_text);
				painter.fillRect(filtered_rect, bg);
				painter.setPen(Qt::red);
				painter.drawText(filtered_rect, Qt::AlignLeft, filtered_text);
			}
		}
	}

}





int File_list_view::focused_item_index() const
{
	return m_focused_item_index;
}





int File_list_view::find_filtered_item(int start, int delta)
{
	while(in_range(start) and not is_filtered(start)) {
		start += delta;
	}
	return start;
}





void File_list_view::set_focused_item_index(int value)
{
	if(value is m_focused_item_index and is_filtered(value)) {
		return;
	}

	if(value >= m_items.length()) {
		value = m_items.length() - 1;
	}
	if(value < 0) {
		value = 0;
	}
	if(not in_range(value)) {
		return;
	}

	if(not is_filtered(value)) {
		int delta = value > m_focused_item_index ? 1 : -1;
		int new_value = find_filtered_item(value, delta);
		if(not in_range(new_value)) {
			new_value = find_filtered_item(value, -delta);
		}
		value = new_value;
	}
	if(in_range(m_focused_item_index)) {
		update_item_view(m_focused_item_index);
	}
	m_focused_item_index = value;
	if(in_range(value)) {
		update_item_view(m_focused_item_index);
	}

	if(has_focused_item()) {
		Item& focused = focused_item();
		if(focused.column < m_left_column) {
			m_left_column = focused.column;
			update();
			return;
		}

		QRect view_rect = rect();
		int save_left_column = m_left_column;
		if(m_left_column < m_columns.length() - 1) {
			int view_right = view_rect.right();
			int column_right = column_view_rect(focused.column).right();
			while((m_left_column < m_columns.length() - 1) and (column_right > view_right)) {
				m_left_column++;
				column_right = column_view_rect(focused.column).right();
			}
		}
		if(save_left_column isnt m_left_column) {
			update();
		}
	}
	emit focused_item_index_changed(m_focused_item_index);
}





void File_list_view::update_item_view(int item_index)
{
	update(item_view_rect(item_index).adjusted(-4, -4, 4, 4));
}





#define sqr(a) ((a)*(a))





int File_list_view::find_nearest_item(int column_delta)
{
	int focused_y = focused_item().rect.top();

	int found = -1;
	for(int col = focused_item().column + column_delta; col >= 0 and col < m_columns.length(); col += column_delta) {
		Wrapper::Strip column = m_columns[col];
		int nearest_index = -1;
		int nearest_distance = 0;
		for(int index = column.first_item_index; index <= column.last_item_index; index++) {
			if(is_filtered(index)) {
				int distance = abs(m_items[index].rect.top() - focused_y);
				if(nearest_index is -1 or distance < nearest_distance) {
					nearest_index = index;
					nearest_distance = distance;
				}
			}
		}
		if(nearest_index >= 0) {
			found = nearest_index;
			break;
		}
	}
	if(found is -1 and column_delta > 0) {
		found = m_items.length();
	}
	return found;
}





void File_list_view::set_filter(QString filter)
{
	if(filter is m_filter) {
		return;
	}
	m_filter = filter;
	update();
	set_focused_item_index(m_focused_item_index);
	emit filter_changed(filter);
}






void File_list_view::keyPressEvent(QKeyEvent *evt)
{
	switch(evt->key()) {
		case Qt::Key_Return:
			evt->ignore();
			break;
		case Qt::Key_Down:
			set_focused_item_index(m_focused_item_index + 1);
			break;

		case Qt::Key_Up:
			set_focused_item_index(m_focused_item_index - 1);
			break;

		case Qt::Key_Home:
			set_focused_item_index(0);
			break;

		case Qt::Key_End:
			set_focused_item_index(m_items_source.length() - 1);
			break;

		case Qt::Key_Left:
		{
			set_focused_item_index(find_nearest_item(-1));
			break;
		}

		case Qt::Key_Right:
		{
			set_focused_item_index(find_nearest_item(1));
			break;
		}

		case Qt::Key_Backspace:
		{
			if(not m_filter.isEmpty()) {
				set_filter(m_filter.left(m_filter.length() - 1));
			}
			else if(!QDir(m_current_path).isRoot()) {
				set_current_path(QDir::cleanPath(m_current_path + "/.."));
			}
			break;
		}

		case Qt::Key_Backslash:
		case Qt::Key_Slash:
		{
			Qt::KeyboardModifiers modifiers = evt->modifiers();
			if(modifiers is Qt::ControlModifier or modifiers is Qt::MetaModifier) {
				set_current_path(QDir::rootPath());
			}
			break;
		}

		default:
			if(not evt->text().isEmpty()) {
				set_filter(m_filter + evt->text());
				return;
			}
			evt->ignore();
			break;
	}
}





//
const QFileInfo* File_list_view::focused_file() const
{
	return in_range(m_focused_item_index) ? &m_items_source[m_focused_item_index] : NULL;
}







