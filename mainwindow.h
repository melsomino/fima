#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "filelistview.h"
#include <QMainWindow>
#include <QTextEdit>
#include <QLineEdit>
#include <QLabel>

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

protected:
	void keyPressEvent(QKeyEvent *evt);
private:
	QLineEdit* m_path_edit;
	QLabel* m_filter_edit;
	File_list_view* m_files_view;
	QTextEdit* m_text_edit;

	void enter_focused_file();
};

#endif // MAINWINDOW_H
