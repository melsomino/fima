#include "mainwindow.h"
#include <QtGui>
#include <QMessageBox>

#define is ==
#define isnt !=


//
void set_color(QWidget* widget, QPalette::ColorRole role, QColor color)
{
	QPalette pal = widget->palette();
	pal.setColor(role, color);
	widget->setPalette(pal);
}




//
MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent)
{
	QColor bg_color(51, 51, 51);
	QColor highlight_color(0xff9358);
	QColor text_color(192, 192, 192);
	QColor filter_color(Qt::red);

	// init window
	set_color(this, QPalette::Background, bg_color);


	// create main
	QWidget* main =new QWidget(this);
	setCentralWidget(main);


	// create current path edit
	m_path_edit = new QLineEdit(main);
	m_path_edit->setText("current path");
	m_path_edit->setFrame(false);
	QFont f = m_path_edit->font();
	f.setFamily("Optima");
	f.setPointSize(24);
	m_path_edit->setFont(f);
	set_color(m_path_edit, QPalette::Base, bg_color);
	set_color(m_path_edit, QPalette::Text, highlight_color);


	// create filter edit
	m_filter_edit = new QLabel(main);
	m_filter_edit->setText("filter");
	set_color(m_filter_edit, QPalette::Foreground, filter_color);


	// create files view
	m_files_view = new File_list_view(main);
	m_files_view->setFocusPolicy(Qt::StrongFocus);


	// create text edit
	m_text_edit = new QTextEdit(main);
	m_text_edit->hide();
	f = m_text_edit->font();
	f.setFamily("Optima");
	f.setPointSize(16);
	m_text_edit->setFont(f);
	m_text_edit->setFrameStyle(0);
	set_color(m_text_edit, QPalette::Base, bg_color);
	set_color(m_text_edit, QPalette::Text, text_color);
	m_text_edit->setReadOnly(true);


	// connect
	QObject::connect(m_files_view, SIGNAL(current_path_changed(QString)), m_path_edit, SLOT(setText(QString)));
	QObject::connect(m_files_view, SIGNAL(filter_changed(QString)), m_filter_edit, SLOT(setText(QString)));


	// layout
	QGridLayout *grid = new QGridLayout(main);
	grid->addWidget(m_path_edit, 0, 0);
	grid->addWidget(m_filter_edit, 0, 1);
	grid->addWidget(m_files_view, 1, 0, 1, 2);
	grid->addWidget(m_text_edit, 1, 0, 1, 2);

	resize(1200,900);
	move(200, 100);


	// start
	m_files_view->set_current_path(QDir::rootPath());
	m_files_view->setFocus();
}




//
MainWindow::~MainWindow()
{
}


void mb(QString text)
{
	QMessageBox b;
	b.setText(text);
	b.exec();
}

//
void MainWindow::enter_focused_file()
{
	const QFileInfo* file = m_files_view->focused_file();
	if(file is NULL) {
		return;
	}

	if(file->isDir()) {
		m_files_view->set_current_path(m_files_view->current_path() + "/" + file->fileName());
		return;
	}

	QFile text_file(file->filePath());
	text_file.open(QIODevice::ReadOnly);
	m_text_edit->setText(QTextStream(&text_file).readAll());
	m_files_view->hide();
	m_text_edit->show();
	m_text_edit->setFocus();
}






//
void MainWindow::keyPressEvent(QKeyEvent *evt)
{
	if(m_path_edit->hasFocus() and evt->key() == Qt::Key_Return and evt->modifiers() == 0) {
		m_files_view->set_current_path(m_path_edit->text());
		m_files_view->setFocus();
		return;
	}

	if(m_files_view->hasFocus() and evt->key() == Qt::Key_Return and evt->modifiers() == 0) {
		enter_focused_file();
		return;
	}

	if(m_files_view->hasFocus() and evt->key() == Qt::Key_Escape and evt->modifiers() == 0) {
		m_files_view->set_filter("");
		return;
	}

	if(m_text_edit->hasFocus() and evt->key() == Qt::Key_Escape and evt->modifiers() == 0) {
		m_text_edit->setText("");
		m_text_edit->hide();
		m_files_view->show();
		m_files_view->setFocus();
		return;
	}

	evt->ignore();
}
