#-------------------------------------------------
#
# Project created by QtCreator 2012-11-08T00:35:29
#
#-------------------------------------------------

QT       += core gui

TARGET = fima
TEMPLATE = app


SOURCES += main.cpp\
		mainwindow.cpp \
	filelistview.cpp

HEADERS  += mainwindow.h \
	filelistview.h

ICON = fima.icns

#QMAKE_CXXFLAGS += -std=c++0x

OTHER_FILES += \
    fima.icns
