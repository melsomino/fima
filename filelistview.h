#ifndef FILELISTVIEW_H
#define FILELISTVIEW_H

#include <QWidget>
#include <QPainter>
#include <QFileInfo>

class Wrapper {
public:
	struct Strip {
		int pos;
		int size;
		int first_item_index;
		int last_item_index;
		Strip(int a_pos, int a_size, int an_item_index):
			pos(a_pos),
			size(a_size),
			first_item_index(an_item_index),
			last_item_index(an_item_index)
		{
		}
	};

	int fixed_size;

	int padding_stretch;
	int padding_fixed;
	int spacing_stretch;
	int spacing_fixed;

	int next_fixed;

	Wrapper(int a_fixed_size, int a_padding_stretch, int a_padding_fixed, int a_spacing_stretch, int a_spacing_fixed):
		fixed_size(a_fixed_size),
		padding_stretch(a_padding_stretch),
		padding_fixed(a_padding_fixed),
		spacing_stretch(a_spacing_stretch),
		spacing_fixed(a_spacing_fixed),
		next_fixed(a_padding_fixed)
	{
	}

	void allocate_space_in_first_new_strip(int size_stretch, int& pos_stretch, int& pos_fixed, QList<Strip>& strips)
	{
		pos_stretch = padding_stretch;
		pos_fixed = padding_fixed;
		strips.append(Strip(pos_stretch, size_stretch, 0));
	}

	void allocate_space_in_next_new_strip(int size_stretch, int& pos_stretch, int& pos_fixed, QList<Strip>& strips)
	{
		Strip& last_strip = strips.last();
		pos_stretch = last_strip.pos + last_strip.size + spacing_stretch;
		pos_fixed = padding_fixed;
		strips.append(Strip(pos_stretch, size_stretch, last_strip.last_item_index + 1));
	}

	void allocate_space_in_last_strip(int size_stretch, int& pos_stretch, int& pos_fixed, QList<Strip>& strips)
	{
		Strip& last_strip = strips.last();
		pos_stretch = last_strip.pos;
		pos_fixed = next_fixed;
		last_strip.last_item_index++;
		if(size_stretch > last_strip.size) {
			last_strip.size = size_stretch;
		}
	}

	void next(int size_stretch, int size_fixed, int& pos_stretch, int& pos_fixed, QList<Strip>& strips)
	{
		if(strips.empty()) {
			allocate_space_in_first_new_strip(size_stretch, pos_stretch, pos_fixed, strips);
		}
		else if(next_fixed + size_fixed > fixed_size - padding_fixed) {
			allocate_space_in_next_new_strip(size_stretch, pos_stretch, pos_fixed, strips);
		}
		else {
			allocate_space_in_last_strip(size_stretch, pos_stretch, pos_fixed, strips);
		}
		next_fixed = pos_fixed + size_fixed + spacing_fixed;
	}
};

// File_list_view
class File_list_view : public QWidget
{
	Q_OBJECT



public:
	explicit File_list_view(QWidget *parent = 0);

	int focused_item_index() const;
	QString current_path() const { return m_current_path; }
	const QFileInfo* focused_file() const;

public slots:
	void set_current_path(QString path);
	void set_focused_item_index(int index);
	void set_filter(QString value);

signals:
	void current_path_changed(QString current_path);
	void focused_item_index_changed(int focused_item_index);
	void filter_changed(QString filter);

protected:
	void paintEvent(QPaintEvent *event);
	void keyPressEvent(QKeyEvent *evt);




private:
	struct Item {
		QString text;
		QRect rect;
		int column;

		Item(const QString& a_text, const QSize& a_size):
			text(a_text),
			rect(QPoint(0, 0), a_size),
			column(0)
		{}
	};



	QSize m_padding;
	QSize m_spacing;

	QFileInfoList m_items_source;
	QList<Item> m_items;
	QList<Wrapper::Strip> m_columns;
	QSize m_view_size_used_for_last_recalc;

	QString m_current_path;

	int m_left_column;
	int m_focused_item_index;
	QString m_filter;


	void set_text_font(QPainter &painter);
	bool has_focused_item() const { return m_focused_item_index >= 0 && m_focused_item_index < m_items.length(); }
	Item& focused_item() { return m_items[m_focused_item_index]; }

	bool is_filtered(int item_index) const;
	bool in_range(int item_index) const;
	QRect rect_to_view(QRect r);
	QRect item_view_rect(int item_index);
	QRect column_view_rect(int column_index);
	QRect column_rect(int column_index);

	int find_filtered_item(int start, int delta);
	int find_nearest_item(int delta);
	void recalc_items_and_columns_if_needed(QSize view_size, QPainter &painter);
	void recalc_items_and_columns_if_needed();
	void recreate_items(QPainter& painter);
	void recalc_layout(QSize view_size);
	void update_item_view(int index);

};

#endif // FILELISTVIEW_H
